class Person(object):

    def __init__(self, name, surname, pesel):
        self.name = name
        self.surname = surname
        self.pesel = pesel

    def get_name(self):
        return self.name

    def get_surname(self):
        return self.surname

    def get_pesel(self):
        return self.pesel

    def __str__(self):
        return "= " + self.name + ":" + self.surname
