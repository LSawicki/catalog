package pl.poznan.put.spio.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.poznan.put.spio.db.DB;

import java.util.List;

@AllArgsConstructor
@Service
public class Catalog {

    private DB db;
    private PESELService service;

    public String getPESEL(String name, String surname) {
        return db.getPESELList(name, surname).get(0);
    }

    public List<String> getPESELList(String name, String surname) {
        return db.getPESELList(name, surname);
    }

    public void addPerson(String name, String surname, String pesel) {
        if (service.verify(pesel)) {
            Person person = new Person(name, surname, pesel);
            db.insertPerson(person);
        } else {
            throw new RuntimeException("improper PESEL");
        }
    }

    public Person getPerson(String pesel) {
        return db.getPerson(pesel);
    }
}
