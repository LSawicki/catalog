import mock as mock
from mock import MagicMock

from catalog.catalog import Catalog
from catalog.db import DB
from catalog.person import Person


def person():
    return Person("imie", "nazwisko", "pesel")


@mock.patch('catalog.db.DB.get_person', MagicMock(return_value=person()))
def test_should_get_person_by_pesel():
    # given
    c = Catalog(DB, None)

    # when
    result = c.get_person("pesel")

    # then
    assert_equal_person(result, person())
    DB.get_person.assert_called_with("pesel")


def assert_equal_person(result, expected):
    assert result.get_name() == expected.get_name()
    assert result.get_surname() == expected.get_surname()
    assert result.get_pesel() == expected.get_pesel()
