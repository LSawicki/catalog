# Ncatalog
Ncatalog jest prostą abstrakcją na katalog osobowy, wspierany przez bazę danych. To nie jest kompletny projekt, tylko szkielet z początkowej fazy prac.

Projekt katalogu został tak przygotowany, że pomimo braku rzeczywistej bazy danych jest on w pełni użytkowy i pozwala pobrać dane z pewnego ograniczonego zakresu. Zaślepka bazy danych została zrealizowana w pliku `mock-catalog.ts`. W docelowym rozwiązaniu ta klasa powinna zostać usunięta.

Testy aplikacji to de facto same testy integracyjne, które uruchamiają fragment lub całą aplikację w mniejszym lub większym stopniu uzupełniając zależności. Testy e2e uruchamiają całość i testują za pośrednictwem żądań do kontrolera.
## Technologie
* TypeScript
* NestJS
* Jest
## Uwagi techniczne
### Preferowane IDE
VS Code
### Budowanie aplikacji
`yarn install`
### Uruchamianie testów
`yarn test`

`yarn test:e2e`
### Uruchamianie aplikacji
`yarn start:dev`

Przykładowe zapytanie: `http://localhost:8080/pesel/A,B`

Przykładowa modyfikacja: `http://localhost:8080/person/C,D,52061716491`
